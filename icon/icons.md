## Icons

This folder contains various icons.

|Icon|Explanation|
|---|---|
|![picture](https://bitbucket.org/shameemsr/images/downloads/spring.png)|Spring|
|![picture](https://bitbucket.org/shameemsr/images/downloads/bitbucket.png)|Bitbucket|
|![picture](https://bitbucket.org/shameemsr/images/downloads/docker.png)|Docker|
